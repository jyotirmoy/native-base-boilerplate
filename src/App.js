import React from 'react';
import { Root } from 'native-base';
import { createDrawerNavigator, createStackNavigator, createAppContainer } from 'react-navigation';

import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';

import { counter } from './reducer';
// A very simple store
let store = createStore(combineReducers({ count: counter }));

import BasicCard from './screens/card/basic';
import NHCardItemBordered from './screens/card/carditem-bordered';
import NHCardItemButton from './screens/card/carditem-button';
import NHCardImage from './screens/card/card-image';
import NHCardShowcase from './screens/card/card-showcase';
import NHCardList from './screens/card/card-list';
import NHCardHeaderAndFooter from './screens/card/card-header-and-footer';
import NHCardTransparent from './screens/card/card-transparent';
import NHCardCustomBorderRadius from './screens/card/card-custom-border-radius';
import Counter from './screens/counter/';

import Home from './screens/home/';

import NHCard from './screens/card/';
import SideBar from './screens/sidebar';

const Drawer = createDrawerNavigator(
  {
    Home: { screen: Home },
    NHCard: { screen: NHCard },
  },
  {
    initialRouteName: 'Home',
    contentOptions: {
      activeTintColor: '#e91e63'
    },
    contentComponent: props => <SideBar {...props} />
  }
);

const AppNavigator = createStackNavigator(
  {
    Drawer: { screen: Drawer },

    BasicCard: { screen: BasicCard },
    NHCardItemBordered: { screen: NHCardItemBordered },
    NHCardItemButton: { screen: NHCardItemButton },
    NHCardImage: { screen: NHCardImage },
    NHCardShowcase: { screen: NHCardShowcase },
    NHCardList: { screen: NHCardList },
    NHCardHeaderAndFooter: { screen: NHCardHeaderAndFooter },
    NHCardTransparent: { screen: NHCardTransparent },
    NHCardCustomBorderRadius: { screen: NHCardCustomBorderRadius },
    Counter: { screen: Counter },
  },
  {
    initialRouteName: 'Drawer',
    headerMode: 'none'
  }
);

// And the app container
const AppContainer = createAppContainer(AppNavigator);

// export default () =>
//   <Root>
//     <AppContainer />
//   </Root>;

  // Render the app container component with the provider around it
export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Root>
          <AppContainer />
        </Root>
      </Provider>
    );
  }
}
