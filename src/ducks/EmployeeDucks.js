// @flow
// import * as firebase from "firebase";

const EMPLOYEE_UPDATE = '@@employee/EMPLOYEE_UPDATE';
const EMPLOYEE_CREATE = '@@employee/EMPLOYEE_CREATE';
const EMPLOYEES_FETCH_SUCCESS = '@@employee/EMPLOYEES_FETCH_SUCCESS';
const EMPLOYEE_SAVE_SUCCESS = '@@employee/EMPLOYEE_SAVE_SUCCESS';

const employeeUpdate = ({ prop, value }) => {
    return {
        type: EMPLOYEE_UPDATE,
        payload: { prop, value }
    };
};

const employeeCreate = ({ name, phone, shift }) => {
    // const { currentUser } = firebase.auth();

    // return (dispatch) => {
    //     firebase.database().ref(`/users/${currentUser.uid}/employees`)
    //         .push({ name, phone, shift })
    //         .then(() => {
    //             dispatch({ type: EMPLOYEE_CREATE });
    //             Actions.employeeList({ type: 'reset' });
    //         });
    // };
};

const employeesFetch = () => {
    // const { currentUser } = firebase.auth();

    // return (dispatch) => {
    //     firebase.database().ref(`/users/${currentUser.uid}/employees`)
    //     .on('value', snapshot => {
    //         dispatch({ type: EMPLOYEES_FETCH_SUCCESS, payload: snapshot.val() });
    //     });
    // };
};

export const employeeSave = ({ name, phone, shift, uid }) => {
    // const { currentUser } = firebase.auth();

    // return (dispatch) => {
    //     firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
    //     .set({ name, phone, shift })
    //     .then(() => {
    //         dispatch({ type: EMPLOYEE_SAVE_SUCCESS });
    //         Actions.employeeList({ type: 'reset' });
    //     });
    // };
};

export const employeeDelete = ({ uid }) => {
    // const { currentUser } = firebase.auth();

    // return () => {
    //     firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
    //     .remove()
    //     .then(() => {
    //         Actions.employeeList({ type: 'reset' });
    //     });
    // };
};

const INITIAL_STATE = {
    name: '',
    phone: '',
    shift: ''
};

function reducer(state: Object = INITIAL_STATE, action: Object): Object {
    switch (action.type) {
    case EMPLOYEE_UPDATE:
        return { ...state, [action.payload.prop]: action.payload.value };
    case EMPLOYEE_CREATE:
        return INITIAL_STATE;
    case EMPLOYEE_SAVE_SUCCESS:
        return INITIAL_STATE;
    case EMPLOYEES_FETCH_SUCCESS:
        return action.payload;
    default:
        return state;
    }
}

const actionCreators = {
    employeeUpdate,
    employeeCreate,
    employeesFetch,
    employeeSave,
    employeeDelete,
};

const actionTypes = {
    EMPLOYEE_UPDATE,
    EMPLOYEE_CREATE,
    EMPLOYEE_SAVE_SUCCESS,
    EMPLOYEES_FETCH_SUCCESS,
};

export {
    actionCreators,
    actionTypes,
};

export default reducer;
