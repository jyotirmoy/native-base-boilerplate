// @flow
// import * as firebase from "firebase";
import Parse from 'parse/react-native';
import { NavigationActions } from 'react-navigation';

const FIELD_CHANGED = '@@auth/FIELD_CHANGED';
const LOGIN_USER_SUCCESS = '@@auth/LOGIN_USER_SUCCESS';
const LOGIN_USER_FAIL = '@@auth/LOGIN_USER_FAIL';
const LOGIN_USER = '@@auth/LOGIN_USER';
const SIGNOUT_USER = '@@auth/SIGNOUT_USER';
const SIGNUP_USER = '@@auth/SIGNOUT_USER';
const SIGNUP_USER_FAIL = '@@auth/SIGNUP_USER_FAIL';
const SIGNUP_USER_SUCCESS = '@@auth/SIGNUP_USER_SUCCESS';

const onFieldChange = (field, value) => {
    console.log({ field, value });
    const payload = {};
    payload[field] = value;
    return {
        type: FIELD_CHANGED,
        payload,
    };
};

const onSignout = () => {
    return (dispatch) => {
        dispatch({ type: SIGNOUT_USER });
        dispatch(NavigationActions.navigate({ routeName: 'AuthScreen' }));
    };
};

const onLogin = (username, password) => {
    return (dispatch) => {
        dispatch({ type: LOGIN_USER });
        const user = Parse.User
            .logIn(username, password).then(function(user) {
                console.log(`User logged in successful with name: ${user.get('username')} and email: ${user.get('email')}`);
                loginUserSuccess(dispatch, user);
        }).catch(function(error){
            console.log(`Error: ${error.code} ${error.message}`);
            loginUserFail(dispatch);
        });
    };
};

const loginUserFail = (dispatch) => {
    dispatch({ type: LOGIN_USER_FAIL });
};

const loginUserSuccess = (dispatch, user) => {
    console.log('logged in user- ', user.isCurrent());
    dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: user
    });
    dispatch(NavigationActions.navigate({ routeName: 'HomeScreen' }));
};

const onSignUp = (firstName, lastName, username, password, email, phone, userType) => {
    console.log({firstName, lastName, username, password, email, phone, userType});
    return (dispatch) => {
        dispatch({ type: SIGNUP_USER });
        // Create a new instance of the user class
        const user = new Parse.User();
        user.set('username', username);
        user.set('password', password);
        user.set('email', email);

        // other fields can be set just like with Parse.Object
        user.set('userType', userType);
        user.set('firstName', firstName);
        user.set('lastName', lastName);
        user.set('phone', phone);

        user.signUp().then(function(user) {
            console.log(`User created successful with name: ${username} and email: ${email}`);
            signupUserSuccess(dispatch, user);
        }).catch(function(error){
            console.log(`Error: ${error.code} ${error.message}`);
            signupUserFail(dispatch);
        });
    };
};

const signupUserFail = (dispatch) => {
    dispatch({ type: SIGNUP_USER_FAIL });
};

const signupUserSuccess = (dispatch, user) => {
    console.log('user- ', user);

    dispatch({
        type: SIGNUP_USER_SUCCESS,
        payload: user
    });
    dispatch(NavigationActions.navigate({ routeName: 'AuthScreen' }));
};

const resetPassword = ({email}) => {
    return (dispatch) => {
        dispatch({ type: LOGIN_USER });
        Parse.User.requestPasswordReset(email).then(function() {
            console.log('Password reset request was sent successfully');
          }).catch(function(error) {
            console.log('The login failed with error: ' + error.code + ' ' + error.message);
          });
    };
};

const INITIAL_STATE = {
    loading: false,
    firstName: '',
    lastName: '',
    username: '',
    password: '',
    email: '',
    phone: '',
    userType: '',
    isAuthenticated: false,
    errorMessage: '',
};

function reducer(state: Object = INITIAL_STATE, action: Object): Object {
    switch (action.type) {
    case SIGNOUT_USER:
        return { ...state, ...INITIAL_STATE };
    case FIELD_CHANGED:
        console.log('action.payload- ', action.payload);
        return { ...state, ...action.payload };
    case LOGIN_USER:
    case SIGNUP_USER:
        return { ...state, loading: true, errorMessage: '' };
    case LOGIN_USER_SUCCESS:
        return { ...state, ...INITIAL_STATE, user: action.payload, loading: false };
    case LOGIN_USER_FAIL:
        return { ...state, errorMessage: 'Authentication Failed.', password: '', loading: false };
    default:
        return state;
    }
}

const actionCreators = {
    onFieldChange,
    onLogin,
    onSignUp,
    onSignout,
    resetPassword,
};

const actionTypes = {
    FIELD_CHANGED,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    LOGIN_USER,
};

export {
    actionCreators,
    actionTypes,
};

export default reducer;
